#include<stdio.h> // define the header file  
#include <stdint.h>
#include <math.h>

#define CCS_EXEC_TIME_MS 10

typedef struct
{
    float *in;      /**< Points to the variable to be filtered */
    float alpha;    /**< See the function declaration. In a nutshell: Alpha = 1-exp(-Ts/tao) */
    float in_last;  /**< Last input is stored in memory */
    float out;      /**< Filtered output of the pointed variable */
    void(*calc)();  /**< Is the function embedded in the structure */
}drv_ccs_low_pass_filter_1o;


/**
 * @brief   Low pass filter embedded
 *          Alpha = 1-exp(-Ts/tau)
 *
 *          Where tau is related to the cutoff frequency; tau = 1/(2 x pi x fc)
 *          Ts is the sampling time (the period of execution time)
 *
 *          Example:
 *              1st order filter of 500Hz executed at 15000Hz:
 *              tau = 1/(2 x pi x 500) = 3.1831e-4
 *              Alpha = 1-exp(-15000/3.1831e-4) = 0.1890
 *
 *          Tip:
 *              Lowering the tau parameter will result in smaller
 *              cutoff frequency. It can be done in real time without
 *              making any further calculations for slow changing systems.
 *
 *              Use the constructor to ease this job
 * @param  Input is a lowpass filter struct
 */
void drv_ccs_low_pass_calc(drv_ccs_low_pass_filter_1o *f)
{
    f->out = (f->alpha * f->in_last) + (( 1.0F - f->alpha ) * f->out)  ;
    f->in_last = *(f->in) ;
}

/**
 * @brief   Constructor for a low pas filter. exp() is heavy and uses doubles, for this reason,
 *          the constructor is only called once and while the processor is not performing critical actions.
 *          Example of usage:
 *          v_target_filter = drv_ccs_construct_low_pass(&v_target, 1.0F/EXEC_TIME_IN_SECONDS, CUTOFF_FREQ);
 * @param   *var         -> Pass the address of the raw variable to filter
 *          exec_freq    -> Is the execution frequency, should be constant for filtering
 *          cut_off_freq -> Is the frequency where the filter has a gain of -3dB
 */
drv_ccs_low_pass_filter_1o drv_ccs_construct_low_pass (float *var, float exec_freq, float cut_off_freq)
{
    drv_ccs_low_pass_filter_1o filter;

    float tau = 1.0F / (2.0F * 3.1415926535897932384626433832795F * cut_off_freq);
    filter.alpha = 1.0F - exp( -(1.0F/exec_freq)/tau );
    filter.in = var;
    filter.in_last = 0.0F;
    filter.out = 0.0F;
    filter.calc = drv_ccs_low_pass_calc;

    return filter;
}

/**
 * @brief   Override the low-pass filter with a value. Not used anymore.
 * @param   Expected input is a drv_ccs_module and the value
 */
void drv_ccs_low_pass_override(drv_ccs_low_pass_filter_1o *f, float var)
{
    f->in_last = var;
    f->out = var;
}


void main()   // define the main function  
{  

    drv_ccs_low_pass_filter_1o v_target_filter;
    float v_target;
    uint16_t i;

    v_target_filter = drv_ccs_construct_low_pass(&(v_target), 1.0F/( (float)CCS_EXEC_TIME_MS * 0.001F ), 0.12F); //0.25F
   

    v_target=500;
    for(i=0;i<1000;i++)
    {
        printf("Iteration %ld \r\n",i);
        v_target_filter.calc(&v_target_filter);
        if(v_target_filter.out>490)
        {
            printf("Target time: %ld alpha:%f \r\n",i*CCS_EXEC_TIME_MS, v_target_filter.alpha );
            break;      
        }
        else
        {
            printf("Time: %ld ms voltage: %f\r\n",i*CCS_EXEC_TIME_MS,v_target_filter.out);
        }

    }
}  
